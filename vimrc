filetype off
"execute pathogen#infect()
set nocompatible

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" my bundles
Plugin 'gregsexton/MatchTag'
"Plugin 'Valloric/YouCompleteMe'
Plugin 'scrooloose/nerdtree'
Plugin 'ervandew/snipmate.vim'
Plugin 'ervandew/supertab'
Plugin 'majutsushi/tagbar'
Plugin 'bling/vim-airline'
"Plugin 'xolox/vim-easytags'
Plugin 'tpope/vim-fugitive'
"Plugin 'https://bitbucket.org/ns9tks/vim-fuzzyfinder'
Plugin 'vim-scripts/FuzzyFinder'
Plugin 'vim-scripts/L9'
"Plugin 'https://bitbucket.org/ns9tks/vim-l9'
Plugin 'http://git.code.sf.net/p/vim-latex/vim-latex'
"Plugin 'xolox/vim-misc'
Plugin 'jlanzarotta/bufexplorer'
Plugin 'nathanaelkane/vim-indent-guides'


" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

syntax enable
colorscheme pencil
set background=dark
set modelines=0
"set number
set gfn=PT\ Mono:h14
set tabstop=4
set shiftwidth=4
set softtabstop=4
"set smartindent
set autoindent
set scrolloff=3
set showmode
set showcmd
set hidden
set wildmenu
set wildmode=longest,list:longest
set conceallevel=2
set concealcursor=vn
set completeopt=menu,menuone,longest
"set completeopt=menu,preview
set visualbell
set cursorline
set ttyfast
set ruler
set backspace=indent,eol,start
set laststatus=2
set relativenumber
set number
set undofile
set wrap
set linebreak
set nolist
"set list
"set listchars=tab:▸\ ,eol:$,trail:~,extends:>,precedes:<
set textwidth=0
set wrapmargin=0
set formatoptions=qrn1
set formatoptions-=t
"set colorcolumn=110
set ignorecase
set smartcase
set gdefault
set hlsearch
set incsearch
set showmatch
set autochdir
set tags=./.tags;
set encoding=utf-8
set mouse=a
set hidden

" Always chdir to the current directory with a file.  Helps with relative paths
"autocmd BufEnter,BufRead,BufNewFile *       lcd %:p:h
"
" Syntax highlight from the beginning of a file (helps with long string blocks)
"autocmd BufEnter,BufRead,BufNewFile *       syntax sync fromstart"
"
" Setup 4 space soft tabs no matter what
"autocmd BufEnter,BufRead,BufNewFile *       set softtabstop=4 shiftwidth=4 expandtab
"autocmd BufEnter,BufRead,BufNewFile *.scala set filetype=scala   

" Find and display overly long lines
" highlight OverLength ctermbg=red ctermfg=white guibg=#592929
" match OverLength /\%121v.\+/

""let g:easytags_file='.tags'
""let g:easytags_cmd = '/opt/local/bin/ctags'
"let g:easytags_dynamic_files = 1
""let g:easytags_events = ['BufEnter', 'BufReadPost', 'BufWritePost']
"let g:easytags_events=['BufWritePost']
""let g:easytags_events=['InsertEnter', 'InsertLeave', 'BufWritePost']
"let g:easytags_python_enabled = 1
"let g:easytags_include_members = 1
"let g:easytags_async = 1
"let g:easytags_autorecurse = 1

noremap <F2> :FufFile<CR>
noremap <F3> :NERDTreeToggle<CR>
let NERDTreeHighlightCursorline=1
noremap <F4> :TagbarToggle<CR>
noremap <F5> :BufExplorer<CR>
noremap <F6> :IndentGuidesToggle<CR>
let g:indent_guides_enable_on_vim_startup=1

"Customizing Tagbar.vim
"This will open the tagbar to the left
let g:tagbar_left = 1
"This will show absolute line numbers in the tagbar
let g:tagbar_show_linenumbers = 0
"This will expand the gui window when tagbar is opened, and possibly also the terminal
let g:tagbar_expand = 2
"To make tagbar wider to the longest tag when zoomed
let g:tagbar_zoomwidth = 0
"To open tagbar automatically on given file types
"autocmd FileType c,cpp,h,hpp nested :TagbarOpen


vnoremap . :norm.<CR>

nnoremap / /\v
vnoremap / /\v

nnoremap <C-]> g<C-]>
"vnoremap <C-]> g<C-]>

let mapleader = ","

"this clears out the search results
nnoremap <leader><space> :noh<cr>
"this matches brackets by simply typing <tab>
nnoremap <tab> %
vnoremap <tab> %


"nnoremap <up> <nop>
"nnoremap <down> <nop>
"nnoremap <left> <nop>
"nnoremap <right> <nop>
"inoremap <up> <nop>
"inoremap <down> <nop>
"inoremap <left> <nop>
"inoremap <right> <nop>

map k gk
map j gj
map <up> gk
map <down> gj
map <C-e> $
map <C-a> 0

"nnoremap j gj
"nnoremap k gk
"vnoremap j gj
"vnoremap k gk
"nnoremap <down> gj
"nnoremap <up> gk
"vnoremap <down> gj
"vnoremap <up> gk
imap <up> <C-o><up>
imap <down> <C-o><down>
inoremap <C-e> <C-o>$
inoremap <C-a> <C-o>0
"nnoremap <C-e> $
"nnoremap <C-a> 0
"vnoremap <C-e> $
"vnoremap <C-a> 0

"inoremap <F1> <ESC>
"nnoremap <F1> <ESC>
"vnoremap <F1> <ESC>

inoremap jj <ESC>

"nnoremap ; :

"For dividing the window vertically and to switch over to it
nnoremap <leader>w <C-w>v<C-w>l

"For moving easily among split windows
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" " IMPORTANT: grep will sometimes skip displaying the file name if you
" " search in a singe file. This will confuse Latex-Suite. Set your grep
" " program to always generate a file-name.
set grepprg=grep\ -nH\ $*

" " OPTIONAL: Starting with Vim 7, the filetype of empty .tex files defaults
" to
" " 'plaintex' instead of 'tex', which results in vim-latex not being loaded.
" " The following changes the default filetype back to 'tex':
let g:tex_flavor='latex'
let g:tex_conceal="adgm" 
let g:Imap_UsePlaceHolders = 0

let g:pencil_higher_contrast_ui = 1   " 0=low (def), 1=high
let g:airline_theme = 'pencil'

""let g:ycm_global_ycm_extra_conf = '~/.vim/bundle/YouCompleteMe/third_party/ycmd/cpp/ycm/my_ycm_extra_conf.py'
""let g:ycm_global_ycm_extra_conf = '.ycm_extra_conf.py'
"let g:ycm_key_invoke_completion = '<tab>'
""let g:ycm_auto_trigger = 0
""let g:ycm_min_num_of_chars_for_completion = 99
"let g:ycm_add_preview_to_completeopt = 1
"let g:ycm_autoclose_preview_window_after_completion = 1
"let g:ycm_collect_identifiers_from_tags_files = 1 
"" make YCM compatible with UltiSnips and Snipmate (using supertab)
"let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
"let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
"let g:SuperTabDefaultCompletionType = '<C-n>'
"let g:ycm_filetype_whitelist = { 'cpp':1, 'c':1, 'h':1, 'hpp':1, 'tex':1 }

" resize current buffer by +/- 5 
 nnoremap <D-left> :vertical resize -5<cr>
 nnoremap <D-down> :resize +5<cr>
 nnoremap <D-up> :resize -5<cr>
 nnoremap <D-right> :vertical resize +5<cr>

map <F10> :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
\ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
\ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>

" load the types.vim highlighting file, if it exists
"autocmd BufRead,BufNewFile *.[ch] let fname = expand('<afile>:p:h') . '.tags'
"autocmd BufRead,BufNewFile *.[ch] if filereadable(fname)
"autocmd BufRead,BufNewFile *.[ch]   exe 'so ' . fname
"autocmd BufRead,BufNewFile *.[ch] endif
