" Additions for more C++ syntax highlighting.
" Tobias Anderberg, 2013.
 
syn keyword cppCustomStatement       nullptr
 
syn keyword cppCustomStlContainers   std::array std::vector std::deque std::forward_list std::list
syn keyword cppCustomStlContainers   std::set std::map std::multiset std::multimap
syn keyword cppCustomStlContainers   std::unordered_set std::unordered_map
syn keyword cppCustomStlContainers   std::unordered_multiset std::unordered_multimap
syn keyword cppCustomStlContainers   std::stack queue std::priority_queue
syn keyword cppCustomStlContainers   std::string
 
syn match   cppCustomParen           "?=(" contains=cParen,cCppParen
syn match   cppCustomFunc            "\w\+\s*(\@=" contains=cppCustomParen
syn match   cppCustomScope           "::"
syn match   cppCustomClass           "\w\+\s*::" contains=cppCustomScope
syn match   cppCustomStlNamespace    "std::\w\+" contains=cppCustomScope
syn match	cppCustomTypes			 "\w\+_t\$"	contains=cppCustomScope
"syn match	cppCustomTypes			 "\(.\+_t\$\|_.\+\)"	contains=cppCustomScope
 
hi def link cppCustomFunc       Function
hi def link cppCustomTypes		CppStlNamespace
hi def link cppCustomStatement  Statement
 
" Custom syntax definitions
hi def link cppCustomClass         CppClassNamespace
hi def link cppCustomStlNamespace  CppStlNamespace
hi def link cppCustomStlContainers CppStlContainer
